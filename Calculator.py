from tkinter import *
from math import *

root=Tk()
string=StringVar()
string.set(" ")
label=Label(root,textvariable=string)
label.grid(row=5,columnspan=3,sticky="n,e,w,s")

def dec():
    string.set(string.get()+".")
def div():
    string.set(string.get()+"/")
def mult():
    string.set(string.get()+"*")
def ded():
    string.set(string.get()+"-")
def sev():
    string.set(string.get()+"7")
def eight():
    string.set(string.get()+"8")
def nine():
    string.set(string.get()+"9")
def add():
    string.set(string.get()+"+")
def four():
    string.set(string.get()+"4")
def five():
    string.set(string.get()+"5")
def six():
    string.set(string.get()+"6")
def empty():
    string.set(" ")
def one():
    string.set(string.get()+"1")
def two():
    string.set(string.get()+"2")
def three():
    string.set(string.get()+"3")
def zero():
    string.set(string.get()+"0")
def equate():
    x=eval(string.get())
    string.set(x)
def perc():
    string.set(string.get()+"%")
def upsc():
    string.set(string.get()+"**")
def dwnsc():
    string.set("sqrt("+string.get()+")")
def calculator():
    buttons=[[".",0,0,dec],["/",0,1,div],["*",0,2,mult],["-",0,3,ded],
        ["7",1,0,sev],["8",1,1,eight],["9",1,2,nine],["+",1,3,add],
        ["4",2,0,four],["5",2,1,five],["6",2,2,six],["clear",2,3,empty],
        ["1",3,0,one],["2",3,1,two],["3",3,2,three],["0",3,3,zero],
        ["=",4,0,equate],["%",4,1,perc],["^",4,2,upsc],["√",4,3,dwnsc]]    
    for button in buttons:
            buttonsFNK=Button(root,text=button[0], command=button[3])
            buttonsFNK.grid(row=button[1],column=button[2],sticky="N,E,W,S")
            
calculator()
root.mainloop()
